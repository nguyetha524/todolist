import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addTodo } from "../redux/todoAction";
import { Text, TextInput, TouchableOpacity, View } from "react-native";
import { RootState } from "../redux/storeTs";

const AddTodoTs: React.FC = () => {
    const [name, setName] = useState<string>('');

    const todo = useSelector( (state: RootState) => state.todo );


    const dispatch = useDispatch();

    const handAddTodo = ():void => {
        if(name !== ''){
            dispatch(addTodo(name))
            
            setName('');
        }
        console.log(todo)
    }

    return(
        <View style={{}}>
            <TextInput
                style={{padding:10, borderWidth:1, margin: 10, borderRadius:10, width: '95%'}}
                placeholder='Name'
                value={name}
                onChangeText={(text) => { setName(text) }} />
            
            <TouchableOpacity
                style={{backgroundColor:'red', alignItems:'center', paddingTop:10, paddingBottom:10, width:100, alignSelf:'center', borderRadius:10}}
             onPress={()=>{handAddTodo()}}> 
                <Text>Submit</Text>
                
            </TouchableOpacity>
        </View>
    )
}
export default AddTodoTs;