import React from 'react';
import { FlatList, Text, View, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';
import { deleteTodo } from '../redux/toDoSlice';
import { useDispatch } from 'react-redux';


const ListTodo = () => {
    const todo = useSelector((state) => state.todo);

    const dispatch = useDispatch();
    // const todos = [];
    const handleDeleteClick = (id, name) => {
        console.log("id "+id)
        dispatch(
            deleteTodo({ id: id, name: name })
        )
    }
    return (
        <View>
            <FlatList
                data={todo}
                renderItem={({ item, index }) => (
                    <View style={{flexDirection:'row', padding:10, borderWidth:1, margin: 10, borderRadius:10}}>
                        <Text style={{padding:5, flex:1}}>{item.id}</Text>
                        <Text style={{padding:5, flex:4}}>{item.name}</Text>
                        
                        <TouchableOpacity 
                        style={{backgroundColor:'red', width: 51, padding:5, borderRadius:10}}
                        onPress={() => { handleDeleteClick(item.id, item.name) }}>
                            <Text>Delete</Text>
                        </TouchableOpacity>
                    </View>
                )}
            />
        </View>
    );
}
export default ListTodo;