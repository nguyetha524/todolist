import React, {FC} from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteTodo } from "../redux/todoAction";

import { View, FlatList, Text, TouchableOpacity } from "react-native";
import { RootState } from "../redux/storeTs";

const ListTodoTs: React.FC = () => {
    const todo = useSelector( (state: RootState) => state.todo );
    const dispatch = useDispatch();

    const handDeleteTodo = (id: number):void => {
        dispatch(deleteTodo(id))
    }

    return (
        <View>
            <FlatList
                data={todo}
                renderItem={({ item, index }) => (
                    <View style={{flexDirection:'row', padding:10, borderWidth:1, margin: 10, borderRadius:10}}>
                        <Text style={{padding:5, flex:1}}>{item.id}</Text>
                        <Text style={{padding:5, flex:4}}>{item.name}</Text>
                        
                        <TouchableOpacity 
                        style={{backgroundColor:'red', width: 51, padding:5, borderRadius:10}}
                        onPress={() => { handDeleteTodo(item.id) }}>
                            <Text>Delete</Text>
                        </TouchableOpacity>
                    </View>
                )}
            />
        </View>
    );
    
}
export default ListTodoTs;