import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { addTodo } from '../redux/toDoSlice';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';

const AddTodo = () => {
    const [name, setName] = useState();

    const dispatch = useDispatch();
    const onSubmit = (event) => {
        event.preventDefault();
        dispatch(
            addTodo({
                name: name,
                
            })
        )
    }
    return (
        // <form onSubmit={onSubmit}>
        //     <input type='text' placeholder='Name' value={name} onChange={(event)=> setName(event.target.value)}/>
        //     <input type='text' placeholder='Age' value={age} onChange={(event)=> setAge(event.target.value)}/>
        //     <button type='submit'>Submit</button>
        // </form>
        <View style={{}}>
            <TextInput
                style={{padding:10, borderWidth:1, margin: 10, borderRadius:10, flex:1}}
                placeholder='Name'
                value={name}
                onChangeText={(text) => { setName(text) }} />
            {/* <TextInput
                placeholder='Age'
                value={age}
                onChangeText={(text) => { setAge(text) }} /> */}
            <TouchableOpacity
                style={{backgroundColor:'red', alignItems:'center', paddingTop:10, paddingBottom:10, width:100, alignSelf:'center', borderRadius:10}}
             onPress={()=>{
                dispatch(
                    addTodo({
                        name: name,
                        
                    })
                )
             }}> 
                <Text>Submit</Text>
            </TouchableOpacity>
        </View>
    )
}
export default AddTodo;