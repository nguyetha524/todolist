import { createSlice } from "@reduxjs/toolkit";
const toDoSlice = createSlice({
    name: "todo",
    initialState:[
        {id: 1, name: 'todo'}
    ],
    reducers:{
        addTodo:(state, action) =>{
            const id = Math.random()
            console.log(id)
            const newTodo ={
                id: id,
                name: action.payload.name
            }
            state.push(newTodo);
        },
        
        deleteTodo:(state, action) =>{
            // state.filter(todo => {
            //     if(todo.id !== action.payload.id){
            //         console.log()
            //     }
            // });
            // const list = []
            // for(let i = 0; i < state.length; i++){
            //     console.log(state[i].id+" "+state[i].name)
            //     console.log("********************************")
            //     if(state[i].id !== action.payload.id){
            //         list.add(state[i])
            //         console.log("lọc "+state[i].id+" "+state[i].name)
            //     }
                
            //     // console.log( "lọc: "+state.filter(todo => {todo.id !== action.payload.id}))
            // }
            console.log(action.payload.id)

            return state.filter(todo => todo.id !== action.payload.id)

        }
    }
})
export const {addTodo, toggleComplete, deleteTodo} = toDoSlice.actions;
export default toDoSlice.reducer;