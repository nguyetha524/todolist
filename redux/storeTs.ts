import { configureStore, createStore } from "@reduxjs/toolkit";
import { todoReducer } from "./todoReducer";

export const storeTs = configureStore({
    reducer:  todoReducer
})

//lấy rootState và appDispatch từ store
export type RootState = ReturnType<typeof storeTs.getState>

export type AppDispatch = typeof storeTs.dispatch