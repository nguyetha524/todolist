import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { toggleComplete, deleteTodo } from './toDoSlice';

const todoItem = ({ id, name }) => {
    const dispatch = useDispatch();

    // const handleCompleteClick = () =>{
    //     dispatch(
    //         toggleComplete({id: id, })
    //     )
    // }
    const handleDeleteClick = () => {
        dispatch(
            deleteTodo({ id: id })
        )
    }

    return (
        <View>
            <Text>{name}</Text>
            <TouchableOpacity onPress={()=>{handleDeleteClick}}>
                <Text>Delete</Text>
            </TouchableOpacity>
        </View>
    )
}
export default todoItem;