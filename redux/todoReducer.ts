import { Action, createReducer, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { addTodo, deleteTodo } from "./todoAction";

interface todo  {
    id: number;
    name: string;
}

interface todoState  {
    todo: todo[]
}

const initialState: todoState ={
    todo: [
        {
            id: 0,
            name: "ha"
        }
    ]
}



export const todoReducer = createReducer( initialState, (builder) => {
    builder
    .addCase(addTodo, (state, action: PayloadAction<string>) => {
        const newTodo: todo ={
            id: state.todo.length + 1,
            name: action.payload
        }
        state.todo.push(newTodo);
    })
    .addCase(deleteTodo, (state, action: PayloadAction<number>) => {
        state.todo = state.todo.filter((todo) => todo.id !== action.payload)
    })
})

// export type RootState = ReturnType<typeof todoReducer>;