import { View, Text } from 'react-native'
import React from 'react'
import AddTodo from './component/AddTodo'
import ListTodo from './component/ListTodo'
import { Provider } from 'react-redux'
import store from './redux/store'

export default function App2() {
  return (
    <View>
      <Text>To do List</Text>
      <Provider store={store}>
        <AddTodo />
        <ListTodo/>
      </Provider>


    </View>
  )
}